package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Created by Dmitriy on 09.05.2016.
 */
@ManagedBean(name = "Number")
@SessionScoped
public class Number implements Serializable {
    private Integer minNumber;
    private Integer maxNumber;
    private Character button;

    public Number() {
    }

    public String next(char button) {
        String result = null;
        switch (button) {
            case 'e': {
                result = "win";
                break;
            }
            case 'l': {
                setButton('l');
                if (Math.abs(getMaxNumber() - getMinNumber()) == 1) {
                    result = "fake";
                } else {
                    setMaxNumber(getNumber());
                }
                break;
            }
            case 'b': {
                setButton('b');
                if (Math.abs(getMaxNumber() - getMinNumber()) == 1) {
                    result = "fake";
                } else {
                    setMinNumber(getNumber());
                }
                break;
            }
        }
        return result;
    }

    public Integer getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(Integer minNumber) {
        this.minNumber = minNumber;
    }

    public Integer getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(Integer maxNumber) {
        this.maxNumber = maxNumber;
    }

    public Integer getNumber() {
        Integer result = null;
        if (Math.abs(getMaxNumber() - getMinNumber()) == 1 && getButton() == 'l') {
            result = getMinNumber();
        }
        if (Math.abs(getMaxNumber() - getMinNumber()) == 1 && getButton() == 'b') {
            result = getMinNumber() + 1;
        } else {
            result = getMinNumber() + (getMaxNumber() - getMinNumber()) / 2;
        }
        return result;
    }

    public Character getButton() {
        return button;
    }

    public void setButton(Character button) {
        this.button = button;
    }
}
